# Azure-K8S-Deployment


### What is this repository for?
This is for creating Kubernetes cluster on Azure

### How do I get set up?
This can't be run locally, we need CircleCI


###  Build Variables

* $PROJECT Project Name : eg. malaga02
* $VNET_PREFIX Subnet : eg. 10.229.244.0
* $VNET_NAME Virtual Network Name : eg. AKS-Production_ExpressRoute
* $VNET_GROUP Subnet resource group : eg. Bestcorp Network
* $AZURE_SUBSCRIPTION Azure Subscription : eg. 6ba2aff9-c62b-4832-90d1-dee7ff987b16
  
### Postman Example
 
 https://circleci.com/api/v1.1/project/bitbucket/bestsellerit/azure-k8s-deployment/tree/master?circle-token=[INSERT TOKEN HERE]
#### Header
`Content-type:application/json`

#### Body

```json
 {
  "build_parameters": {
    "CIRCLE_JOB": "azure-deploy-ingress-lb",
	"PROJECT": "malaga02",
	"VNET_PREFIX" : "10.229.244.0",
	"VNET_NAME":"AKS-Production_ExpressRoute",
	"VNET_GROUP":"BestcorpNetwork",
	"AZURE_SUBSCRIPTION":"6ba2aff9-c62b-4832-90d1-dee7ff987b16"
  }
}
```


### CircleCI Variables
* AZURE_K8S_MASTER_KEY
* AZURE_K8S_MASTER_STORAGE_KEY
* BLOB_CIRCLE_TOKEN

### Who do I contact?

* Engineering Services
