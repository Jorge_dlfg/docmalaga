terraform {
  backend "azurerm" {
  }
}

provider "azurerm" {
  version         = "~> 1.18"
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_resource_group" "k8s" {
  name     = "${var.resource_group_name}"
  location = "${var.location}"
}

module "aks" {
  source = "modules/aks"

  client_id                   = "${var.client_id}"
  client_secret               = "${var.client_secret}"
  subscription_id             = "${var.subscription_id}"
  tenant_id                   = "${var.tenant_id}"
  location                    = "${azurerm_resource_group.k8s.location}"
  resource_group_name         = "${azurerm_resource_group.k8s.name}"
  ssh_public_key              = "${var.ssh_public_key}"
  agent_count                 = "${var.agent_count}"
  dns_prefix                  = "${var.dns_prefix}"
  cluster_name                = "${var.cluster_name}"
  subnet_address_prefix       = "${var.subnet_address_prefix}"
  virtual_network_name        = "${var.virtual_network_name}"
  subnet_resource_group_name  = "${var.subnet_resource_group_name}"
  network_service_cidr        = "${var.network_service_cidr}"
  network_dns_service_ip      = "${var.network_dns_service_ip}"
  network_docker_bridge_cidr  = "${var.network_docker_bridge_cidr}"
}

module "acr" {
  source = "modules/acr"

  location                  = "${azurerm_resource_group.k8s.location}"
  resource_group_name       = "${azurerm_resource_group.k8s.name}"
  container_registry_name   = "${var.container_registry_name}"
  client_id                 = "${var.client_id}"
  client_secret             = "${var.client_secret}"
  subscription_id           = "${var.subscription_id}"
  tenant_id                 = "${var.tenant_id}"

}