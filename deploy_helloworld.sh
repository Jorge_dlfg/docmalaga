#!/bin/bash
set -e # set is triggered to handle exit codes automatically.
STORAGE_ACCOUNT="k8smasterstorage"

# Download blob downloader
curl $(curl https://circleci.com/api/v1.1/project/bitbucket/bestsellerit/blob-downloader/latest/artifacts\?circle-token\=$BLOB_CIRCLE_TOKEN | grep -o 'https://[^"]*')"?circle-token=$BLOB_CIRCLE_TOKEN" --output blob-downloader
chmod +x blob-downloader
ls -l
echo $STORAGE_ACCOUNT
echo $AZURE_K8S_MASTER_STORAGE_KEY
echo $PROJECT
echo $BLOB_CIRCLE_TOKEN
# Download service principal key
./blob-downloader \
    --account $STORAGE_ACCOUNT \
    --accesskey $AZURE_K8S_MASTER_STORAGE_KEY \
    --url https://$STORAGE_ACCOUNT.blob.core.windows.net/project-keys/k8s-$PROJECT.json \
    --project $PROJECT \
    --output k8s-$PROJECT.json

# Assign vars
CLIENT_ID="$(jq -r '.appId' k8s-$PROJECT.json)"
CLIENT_SECRET="$(jq -r '.password' k8s-$PROJECT.json)"
TENANT_ID="$(jq -r '.tenant' k8s-$PROJECT.json)"

# Loggin into Azure CLI
az login --service-principal -u $CLIENT_ID --password $CLIENT_SECRET --tenant $TENANT_ID
az account set --subscription $AZURE_SUBSCRIPTION
az aks get-credentials --resource-group $PROJECT --name $PROJECT-k8s
az aks install-cli

