#!/bin/bash
set -e # set is triggered to handle exit codes automatically.
STORAGE_ACCOUNT="k8smasterstorage"

# Download blob downloader
curl $(curl https://circleci.com/api/v1.1/project/bitbucket/bestsellerit/blob-downloader/latest/artifacts\?circle-token\=$BLOB_CIRCLE_TOKEN | grep -o 'https://[^"]*')"?circle-token=$BLOB_CIRCLE_TOKEN" --output blob-downloader
chmod +x blob-downloader

# Download service principal key
./blob-downloader \
    --account $STORAGE_ACCOUNT \
    --accesskey $AZURE_K8S_MASTER_STORAGE_KEY \
    --url https://$STORAGE_ACCOUNT.blob.core.windows.net/project-keys/k8s-$PROJECT.json \
    --project $PROJECT \
    --output k8s-$PROJECT.json

# Assign vars
CLIENT_ID="$(jq -r '.appId' k8s-$PROJECT.json)"
CLIENT_SECRET="$(jq -r '.password' k8s-$PROJECT.json)"
TENANT_ID="$(jq -r '.tenant' k8s-$PROJECT.json)"

# Loggin into Azure CLI
az login --service-principal -u $CLIENT_ID --password $CLIENT_SECRET --tenant $TENANT_ID
az account set --subscription $AZURE_SUBSCRIPTION
az aks get-credentials --resource-group $PROJECT --name $PROJECT-k8s
az aks install-cli

# Deploy ingress depolyments, cluster roles etc.
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml

# Deploy ingress service.
kubectl apply -f internal-ingress.yaml

# Clone es-config-files repo
git clone git@bitbucket.org:bestsellerit/es-configuration-files.git
     
# Install envsubt 
apk update
apk add gettext
     
# replace strings (Insert in Humio token)
envsubst < ./es-configuration-files/fluentbit/fluentbit-ds.yml > ./es-configuration-files/fluentbit/fluentbit-ds_vartest.yml

# Deploy for fluentbit (The order of the files is mandatory)
kubectl apply -f ./es-configuration-files/fluentbit/fluentbit-sa-role.yml
kubectl apply -f ./es-configuration-files/fluentbit/fluentbit-configmap.yml
kubectl apply -f ./es-configuration-files/fluentbit/fluentbit-ds_vartest.yml

