variable "resource_group_name" {
  description = "Resource group name"
}

variable "location" {
  description = "the region in which the infrastructure should be deployed"
}

variable "container_registry_name" {
  description = "name of the container registry"
}

variable "client_id" {
  description = "The Client ID of the Service Principal used by this Managed Kubernetes Cluster"
}

variable "client_secret" {
  description = "The Client Secret of the Service Principal used by this Managed Kubernetes Cluster"
}

variable "subscription_id" {
  description = "The Subscription ID of the project used by this Managed Kubernetes Cluster"
}

variable "tenant_id" {
  description = "The Tenant ID of the Service Principal used by this Managed Kubernetes Cluster"
}