variable "agent_count" {
    description = "The amount of nodes in the node pool"
}

variable "ssh_public_key" {
    description = "path to the ssh key e.g. '~/.ssh/id_rsa.pub'"
}

variable "dns_prefix" {
    description = "Dns prefix of the AKS instance"
}

variable "resource_group_name" {
  description = "Resource group name"
}
variable "location" {
  description = "the region in which the infrastructure should be deployed"
}
variable "cluster_name" {
  description = "name of the cluster"
}

variable "client_id" {
  description = "The Client ID of the Service Principal used by this Managed Kubernetes Cluster"
}

variable "client_secret" {
  description = "The Client Secret of the Service Principal used by this Managed Kubernetes Cluster"
}

variable "subscription_id" {
  description = "The Subscription ID of the project used by this Managed Kubernetes Cluster"
}

variable "tenant_id" {
  description = "The Tenant ID of the Service Principal used by this Managed Kubernetes Cluster"
}

variable "virtual_network_name" {
  description = "The name of the virtual network to which to attach the subnet."
}

variable "subnet_address_prefix" {
  description = "The address prefix to use for the subnet."
}

variable "subnet_resource_group_name" {
  description = "he name of the resource group in which to create the subnet. (Must be the VNET group as well)"
}


variable "network_service_cidr" {
  description = "The Network Range used by the Kubernetes service"
}

variable "network_dns_service_ip" {
  description = "IP address within the Kubernetes service address range that will be used by cluster service discovery (kube-dns)"
}

variable "network_docker_bridge_cidr" {
  description = "IP address (in CIDR notation) used as the Docker bridge IP address on nodes"
}
