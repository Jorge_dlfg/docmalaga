provider "azurerm" {
  version = "~> 1.18"
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_subnet" "k8s-subnet" {
  name                 = "${var.cluster_name}"
  address_prefix       = "${var.subnet_address_prefix}"
  virtual_network_name = "${var.virtual_network_name}"
  resource_group_name  = "${var.subnet_resource_group_name}"
}

resource "azurerm_kubernetes_cluster" "k8s-internal" {
  name                = "${var.cluster_name}"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"
  dns_prefix          = "${var.dns_prefix}"
  kubernetes_version  = "1.11.3"

  agent_pool_profile {
    name            = "default"
    count           = "${var.agent_count}"
    vm_size         = "Standard_DS2_v2"
    os_type         = "Linux"
    os_disk_size_gb = 30
    vnet_subnet_id  = "${azurerm_subnet.k8s-subnet.id}"
  }

  service_principal {
    client_id     = "${var.client_id}"
    client_secret = "${var.client_secret}"
  }

  network_profile {
    network_plugin     = "azure"
    service_cidr       = "${var.network_service_cidr}"
    dns_service_ip     = "${var.network_dns_service_ip}"
    docker_bridge_cidr = "${var.network_docker_bridge_cidr}"
  }

  tags {
    Environment = "Development"
  }
}
