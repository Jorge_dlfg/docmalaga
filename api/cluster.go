package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

//*********
// SQLite3 database creation and automatic schema migration
//*********

var db *gorm.DB

type Cluster struct {

	// We use Model to let gorm know that this is going to be modelled in the database
	gorm.Model
	ProjectName  string
	ContactEmail string
}

func initialMigration() {
	db, err := gorm.Open("sqlite3", "clusterRegistry.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("Error: Failed to connect database!")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Cluster{})
}

//*********
// List All Clusters endpoint
// Description: query all Cluster records within database and then encode this into JSON and return this as the response.
//*********

func allClusters(w http.ResponseWriter, r *http.Request) {

	db, err := gorm.Open("sqlite3", "clusterRegistry.db")
	if err != nil {
		panic("Error: Failed to connect database!")
	}
	defer db.Close()

	var clusters []Cluster
	db.Find(&clusters)
	fmt.Println("{}", clusters)

	json.NewEncoder(w).Encode(clusters)
}

//*********
// New Cluster endpoint
// Description: insert a new cluster into database. This will need to parse both a cluster name and a contact email from the query parameters of the request made to our API.
//*********

func newCluster(w http.ResponseWriter, r *http.Request) {

	fmt.Println("New Cluster hit")

	db, err := gorm.Open("sqlite3", "clusterRegistry.db")
	if err != nil {
		panic("Error: Failed to connect database!")
	}
	defer db.Close()

	vars := mux.Vars(r)
	projectName := vars["projectName"]
	contactEmail := vars["contactEmail"]

	fmt.Println(projectName)
	fmt.Println(contactEmail)

	db.Create(&Cluster{ProjectName: projectName, ContactEmail: contactEmail})
	fmt.Fprintf(w, "New Cluster Successfully Added to Registry")
}

//*********
// Delete Cluster endpoint
// Description: deletes a cluster record from the database that matches the same name passed into it via a path parameter.
// Note: doesn�t handle the cases where more than one cluster exists within the database with the same name.
//*********

func deleteCluster(w http.ResponseWriter, r *http.Request) {

	db, err := gorm.Open("sqlite3", "clusterRegistry.db")
	if err != nil {
		panic("Error: Failed to connect database!")
	}
	defer db.Close()

	vars := mux.Vars(r)
	projectName := vars["projectName"]

	var cluster Cluster
	db.Where("projectName = ?", projectName).Find(&cluster)
	db.Delete(&cluster)

	fmt.Fprintf(w, "Successfully Deleted Cluster from Registry")
}

//*********
// Update Cluster endpoint
// Description: updates a cluster record from the database that matches the same name passed into it via a path parameter.
//*********

func updateCluster(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open("sqlite3", "clusterRegistry.db")
	if err != nil {
		panic("Error: Failed to connect database!")
	}
	defer db.Close()

	vars := mux.Vars(r)
	projectName := vars["projectName"]
	contactEmail := vars["contactEmail"]

	var cluster Cluster
	db.Where("projectName = ?", projectName).Find(&cluster)

	cluster.ContactEmail = contactEmail

	db.Save(&cluster)
	fmt.Fprintf(w, "Successfully Updated Cluster in Registry")
}
