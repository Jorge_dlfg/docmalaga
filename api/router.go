package api

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

//*********
// API creation with 4 CRUD endpoints: return all users, add a new user, delete a user and update a user.
//*********

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/clusters", allClusters).Methods("GET")
	myRouter.HandleFunc("/cluster/{projectName}", deleteCluster).Methods("DELETE")
	myRouter.HandleFunc("/cluster/{projectName}/{email}", updateCluster).Methods("PUT")
	myRouter.HandleFunc("/cluster/{projectName}/{email}", newCluster).Methods("POST")
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}
