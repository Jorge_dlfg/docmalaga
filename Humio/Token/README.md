## Create HUMIO Token.
A Humio Token must be created in order to have a link between Humio and Postman and be able to see the logs registered by FluentBit.
### Steps to reproduce.
#### - 	First, in ‘Settings’ template, click on “Ingest API Tokens” on the left side of the page.
![Humio Ingest API Tokens](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/ae745ee30662e621f773892d34a6c5f8cd6eaaef/Images/humioingestAPItokens.jpg)

#### - Insert a “Token Name” and then click on “Create Token” button. Later, you can see the Token Key clicking on the “eye” icon displayed in the token’s list.
![Humio New Token](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/0e49e89b12e5b19785bd8f40dcadf22d49f92ce2/Images/HumioNewToken.jpg)

![Humio Token Key](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/0f1884ac648a4ab2382fe124c92662a594569f96/Images/HumioNewToken2.jpg)