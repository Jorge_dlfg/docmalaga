## Create HUMIO Repository.
A Humio repository must be created in order to display the logs registered by FluentBit.
### Steps to reproduce.
#### - Go to https://www.humio.com/ and sign in with your mail address.
#### - Click on "Add Item" and then, click on "Repository" ("Trial Repository" if we   have not a Pro Account). -> The next template is displayed.

![Humio Login Template](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/4b47232eacdc0397fa008009347b610e9962328b/Images/humiologin.jpg)

#### - Set a "Name" and add a "Description" and then click on "Create Repository". Once the repository is created, the user who has implemented it, is added by default. In addition, you can add as many users as necessary. 

![Humio New Repository](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/a06e293125067c046602994b276ca50b8b54f9ff/Images/humionewrepository.jpg)


![Humio Add User](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/4dd1a50e3e3163a922219809cbfd9bdadeb05963/Images/humioadduser.jpg)


  
  