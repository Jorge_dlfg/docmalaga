#!/bin/sh
set -e # set is triggered to handle exit codes automatically.
STORAGE_ACCOUNT="k8smasterstorage"

# Download blob downloader
curl $(curl https://circleci.com/api/v1.1/project/bitbucket/bestsellerit/blob-downloader/latest/artifacts\?circle-token\=$BLOB_CIRCLE_TOKEN | grep -o 'https://[^"]*')"?circle-token=$BLOB_CIRCLE_TOKEN" --output blob-downloader
chmod +x blob-downloader

# Download storage account key
./blob-downloader \
    --account $STORAGE_ACCOUNT \
    --accesskey $AZURE_K8S_MASTER_STORAGE_KEY \
    --url https://$STORAGE_ACCOUNT.blob.core.windows.net/project-keys/$PROJECT-storage-keys \
    --project $PROJECT \
    --output $PROJECT-storage-keys.json

# Assign access key to var
ACCESS_KEY="$(jq -r '.[0].value' $PROJECT-storage-keys.json)"

# Download service principal key
./blob-downloader \
    --account $STORAGE_ACCOUNT \
    --accesskey $AZURE_K8S_MASTER_STORAGE_KEY \
    --url https://$STORAGE_ACCOUNT.blob.core.windows.net/project-keys/k8s-$PROJECT.json \
    --project $PROJECT \
    --output k8s-$PROJECT.json

# Assign vars
CLIENT_ID="$(jq -r '.appId' k8s-$PROJECT.json)"
CLIENT_SECRET="$(jq -r '.password' k8s-$PROJECT.json)"
TENANT_ID="$(jq -r '.tenant' k8s-$PROJECT.json)"

# Initialaizing the back-end
terraform init \
    -backend-config="storage_account_name=$PROJECT""storage" \
    -backend-config="container_name=$PROJECT-tfstate" \
    -backend-config="key=$PROJECT.tfstate" \
    -backend-config="access_key="$ACCESS_KEY""

# 24 is the size of the subnet
terraform apply -auto-approve \
    -var "subscription_id=$AZURE_SUBSCRIPTION" \
    -var "client_id=$CLIENT_ID" \
    -var "client_secret=$CLIENT_SECRET" \
    -var "tenant_id=$TENANT_ID" \
    -var "cluster_name=$PROJECT-k8s" \
    -var "container_registry_name=$PROJECT""k8sregistry" \
    -var "resource_group_name=$PROJECT" \
    -var "virtual_network_name=$VNET_NAME" \
    -var "subnet_address_prefix=$VNET_PREFIX/24" \
    -var "subnet_resource_group_name=$VNET_GROUP"