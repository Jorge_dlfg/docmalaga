#!/bin/bash
set -e # set is triggered to handle exit codes automatically.
LOCATION="westeurope" # It is the location for the storage account use for terraform
STORAGE_ACCOUNT="k8smasterstorage"

# Create Service Principal key
echo $AZURE_K8S_MASTER_KEY | base64 -d > k8s-master.json

# Login with master account
az login --service-principal -u $(jq -r '.appId' k8s-master.json) -p $(jq -r '.password' k8s-master.json) --tenant $(jq -r '.tenant' k8s-master.json)

# Connect to the correct subscription
az account set --subscription $AZURE_SUBSCRIPTION

# Create a storage resource group
az group create \
  --name $PROJECT-storage \
  --location $LOCATION

# Create a storage account
az storage account create \
    --name "$PROJECT"storage \
    --resource-group $PROJECT-storage \
    --location $LOCATION \
    --sku Standard_LRS \
    --kind StorageV2

# Get and store access keys
az storage account keys list --account-name "$PROJECT"storage --resource-group $PROJECT-storage > $PROJECT-storage-keys.json
az storage blob upload -f $PROJECT-storage-keys.json -c project-keys -n $PROJECT-storage-keys --account-name $STORAGE_ACCOUNT --account-key $AZURE_K8S_MASTER_STORAGE_KEY

# Create storage container for Terraform state
az storage container create -n $PROJECT-tfstate --account-name "$PROJECT"storage --account-key $(jq -r '.[0].value' $PROJECT-storage-keys.json)

# Create k8s service principal
if [[ ! $(az ad sp list --spn "http://k8s-$PROJECT" -o tsv) ]];
  then
    az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/$AZURE_SUBSCRIPTION" --name k8s-$PROJECT > k8s-$PROJECT.json
    az storage blob upload -f k8s-$PROJECT.json -c project-keys -n k8s-$PROJECT.json --account-name $STORAGE_ACCOUNT --account-key $AZURE_K8S_MASTER_STORAGE_KEY
fi