package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"bitbucket.org/Jorge_dlfg/docmalaga/api"
)

func main() {
	fmt.Println("BS Engineering Services - Cluster Registry")

	// Create DB
	api.initialMigration()

	// Handle Subsequent requests
	api.handleRequests()
}