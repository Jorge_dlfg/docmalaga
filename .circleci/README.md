# Create Cluster with new Pipeline.
## __Config.yml__
#### Alpine Linux distribution is used for manage our images. For that reason, Alpine Docker image is selected.
Config.yml file must be created to define the next premises:

<span style="color:green"> **Azure setup:**</span> 
    
    Setup_azure.sh file is executed from config.yml.
        
    Setup_azure.sh provide the following...:
    
    - Create Service Principal key
    - Login with master account
    - Connect to the correct subscription
    - Create a storage resource group
    - Create a storage account
    - Get and store acess keys
    - Create k8s service principal
  
<span style="color:green">**Azure_create_cluster:**</span> 
  
   This workflow is responsible for creating the cluster.
   
   Jq language is a JSON parser required for managing JSON files.
   
   For installing jq the following commands are used:
 
```yaml
    command: |
                  apk update
                  apk add jq
```
   Then, Alpine needs a link to be able to execute a piece of "C" responble of starting the GO application that is used for the cluster creation.
   
   The commands which enable this are showed below:

```yaml
    command: |
             mkdir /lib64
             ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2  
```

   The last block executes the file run_terraform.sh from Config.yml which is responsible for creating the cluster.

```yaml
    command: |
             /bin/sh run_terraform.sh
```

   run_terraform.sh file provide the following...: 
    
 - Download blob downloader 
 - Download blob downloader storage account key    
 - Assign access key to var
 - Download service principal key
 - Assign vars
 - Initializing the back-end
 - Enable build variables
   
<span style="color:green">**Azure-deploy-ingress-lb:**</span> 
   
   This workflow is responsible for deploying the ingress. **The purpose of the ingress, is to manage external access to the services running in the cluster without each service needing to have its own public IP.**
   
   As in previous cases, Alpine needs a link to be able to execute a piece of "C" responsible of starting the GO application that is used for the cluster creation.

   The commands which enable this:
```yaml
   command: |
            mkdir /lib64
            ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2   
```
   The second step is to deploy the ingress load balancer and DNS stub domain. Load balancer is used to restrict access to applications in Azure Kubernetes Service (AKS).
   <span style="color:red">**It allows that a Kubernetes service to be accessible only for applications that run in the same virtual network at the same Kubernetes cluster.**</span>

   The file responsible for conducting the deployment of the ingress load balancer is deploy_ingress_lb.sh and provide the following...:
   
   - Download blob downloader
   - Download service principal key
   - Assign vars
   - Login into Azure CLI
   - Set Azure Subscription
   - Get credentials to use the cluster
   - Install the Azure CLI
   - Deploy ingress deployments, cluster roles, etc.
   - Deploy ingress service
   - Clone es-config-files repo
   - Install envsubt
   - Replace Strings (Insert in Humio Token)
   - Deploy for FluentBit (The order of the files is mandatory)
     
```yaml
   command: |
             kubectl apply -f./es-configuration-files/fluentbit/fluentbit-sa-role.yml
             kubectl apply -f./es-configuration-files/fluentbit/fluentbit-configmap.yml
             kubectl apply -f./es-configuration-files/fluentbit/fluentbit-ds_var.yml
```

   Then, it is applied the DNS stub domain described in dns_stub_domain.yaml file with the following command:      

```yaml
   command: |
             kubectl apply -f dns_stub_domain.yaml
```

<span style="color:green">**Azure-deploy-helloworld:**</span> 
   
   This workflow is responsible for deploying a small script. 
  
   As in previous cases, Alpine needs a link to be able to execute a  piece of “C” charge of starting the GO application that is used for the cluster creation.
  
   The commands which enable this:

```yaml
   command: |
            mkdir /lib64
            ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
```
   The second step is to deploy the script called "Helloworld". The file responsible for conducting the deployment of the ingress load balancer is deploy_helloworl.sh and provide the following...:

   - Download blob downloader
   - Download service proncipal key
   - Assign vars
   - Login into Azure subscription
   - Get credentials to use the cluster
   - Install Azure CLI
  
   Then, it is required to run "get the context" command to use the cluster properly with the following command:

```yaml
   command: |
             kubectl config get-contexts
```
   Finally, the azure_helloworld.yaml contains the script previously described.
  
   At the end of the config.yml file, it is configured the webhook which allow to receive notifications whenever particular changes has been made in the repository.

```yaml
   command: |
             notify:
             webhooks:
              - url: https://webhook.site/4c565715-3eef-4d91-8c89-609abd18c9b0
```