variable "agent_count" {
    default = 3
}

variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
}

variable "dns_prefix" {
    default = "k8stest"
}

variable "resource_group_name" {
  default = "pbtest01"
}
variable "location" {
  default = "West Europe"
}
variable "cluster_name" {
  default = "pbtest01-k8s"
}

variable "subnet_address_prefix" {
  default = ""
}

variable "virtual_network_name" {
  default = ""
}

variable "subnet_resource_group_name" {
  default = ""
}

variable "network_service_cidr" {
  default = "172.17.8.0/21"
}

variable "network_dns_service_ip" {
  default = "172.17.8.2"
}

variable "network_docker_bridge_cidr" {
  default = "172.17.0.1/21"
}


variable "container_registry_name" {
  default = "pbtest01k8sregistry"
}
