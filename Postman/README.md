#### POSTMAN POST REQUEST SETUPP
Once the postman application has started, the main menu is displayed with a 'GET' request selected by default

![GetRequest](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/e7e7b85cb7a3f50fe09e804aa3244cacab19ff70/Images/Postman_Get_Request_by_default.jpg)

Then, click on "Requests" dropdown and select 'Post'

![RequestDropdown](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/e7e7b85cb7a3f50fe09e804aa3244cacab19ff70/Images/Post_Request.jpg)

First, it is must fill in the 'Params' tab, specifically 'Key', 'Value' and 'URL' fields.

'Key' is the name of the token variable (eg. circle-token).
'Value' must be the specific API token provided by CircleCI --> **NOTE: CircleCI Token should have 'All' scope in Settings**
 (eg. 4507201f23eec191760e0c9776c7090464678122)
 'URL' is splitted into several parts.
  
   - "https://circleci.com" - **the resource URL for the API being called**
   - api - **the class being called**
   - v1.1 - **the API version**
   - vcs-type - **is a placeholder variable and refers to your chosen VCS (either github or bitbucket)**
   - org - **is a placeholder variable and refers to the name of your CircleCI organization**
   - repo - **is a placeholder variable and refers to the name of your repository**
   - tree
   - branch - **is a placeholder variable and refers to the name of your branch**

![PostmanParams](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/e7e7b85cb7a3f50fe09e804aa3244cacab19ff70/Images/Postman_Params.jpg)

Then, go to 'Headers' tab and complete 'Key' and 'Value' fields.

  - "Key" must be 'Content-type'
  - "Value" must be 'application/json'

![PostmanHeader](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/e7e7b85cb7a3f50fe09e804aa3244cacab19ff70/Images/Postman_Header.jpg)

Finally, it is configured the 'Body' section. In this example, you need to add the build parameters that CircleCI is going to manage in order to build the project.

The build parameters included in this project are the following:
  
  - "CIRCLE_JOB": **"NAME OF THE CURRENT WORKFLOW"**
  - "PROJECT": **"PROJECT NAME"**
  - "VNET_PREFIX": **"SUBNET PROVIDED"**
  - "VNET_NAME": **"SUBNET NAME"**
  - "VNET_GROUP": **"SUBNET GROUP"**
  - "AZURE_SUBSCRIPTION": **"AZURE SUBSCRIPTION (Always the same)"** 
  - "HUMIO_TOKEN": **"TOKEN PROVIDED BY HUMIO AFTER HUMIO REPO CREATION"**

Once you click on 'Send' to send the request, if everything goes fine, it is received **"201 Created"** Status.

![PostmanRequestStatus](https://bitbucket.org/Jorge_dlfg/docmalaga/raw/3e266fc6367eb556886d2463ac3190d51437ceab/Images/Postman_Status.jpg)