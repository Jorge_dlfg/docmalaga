# ACR MODULE
output "acr_client_key" {
    value = "${module.acr.client_key}"
}

# AKS MODULE
output "aks_client_key" {
    value = "${module.aks.client_key}"
}

output "aks_client_certificate" {
    value = "${module.aks.client_certificate}"
}

output "aks_cluster_ca_certificate" {
    value = "${module.aks.cluster_ca_certificate}"
}

output "aks_cluster_username" {
    value = "${module.aks.cluster_username}"
}

output "aks_cluster_password" {
    value = "${module.aks.cluster_password}"
}

output "aks_kube_config" {
    value = "${module.aks.kube_config}"
}

output "aks_host" {
    value = "${module.aks.host}"
}